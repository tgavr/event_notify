require 'test_helper'

class CoreTest < ActiveSupport::TestCase
  def test_event_triggering
    expected = {status: 'sent'}
    assert_equal expected, Event.trigger(:test_event, text: 'triggered', lol: 'wtf')
  end
end