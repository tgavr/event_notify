class UserSubscriptions < ActiveRecord::Migration
  def change
    add_column :users, :subscriptions, :integer, array: true, default: []
  end
end
