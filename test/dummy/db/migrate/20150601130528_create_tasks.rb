class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :text
      t.integer :owner_id
      t.datetime :remind_at

      t.timestamps null: false
    end
  end
end
