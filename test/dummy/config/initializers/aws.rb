require 'aws-sdk-v1'

AWS.config(
    :region => 'eu-west-1',
    :ses => { :region => 'eu-west-1' },
    :access_key_id => ENV['AWS_ACCESS'],
    :secret_access_key => ENV['AWS_SECRET']
)