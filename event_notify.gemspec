$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "event_notify/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "event_notify"
  s.version     = EventNotify::VERSION
  s.authors     = [""]
  s.email       = ["censoredp@gmail.com"]
  s.homepage    = ""
  s.summary     = "Notify subscriptors with triggers"
  s.description = "Rails plugin - singleton for simple triggering events from anywere of app and send notification mails to subscriptors"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0"

  s.add_development_dependency "sqlite3"
end
