class EventMailer < ActionMailer::Base
  def notify(type, params, emails)
    mail(to: emails, 
         subject: "CRM:"+I18n.t("activerecord.attributes.event.type_#{type.id2name}"),
         from: "info@crm.digital-mind.ru") do |format|
      format.html { render "event_notify/#{type.id2name}", locals: {params: params} }
      format.text { render text: 'test' }
    end
  end
end