module Event
  def self.trigger *args
    subject = 'test'
    body = 'test'

    options = args.extract_options!

    if (emails = User.where("#{EVENT_TYPES[args[0]]} = ANY(subscriptions)").pluck(:email)).present?
      EventMailer.notify(args[0], options, emails).deliver_now!
      return {status: 'sent'}
    else
      return {status: 'no users subscripted'}
    end
  end
end